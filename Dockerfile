FROM golang:1.17

WORKDIR $GOPATH/src
COPY . .

RUN go mod tidy -v
RUN ls

ENTRYPOINT ["go", "run", "./cmd/"]