module gitlab.com/NaumaN-Sikrii/balance-api

go 1.16

require (
	github.com/btcsuite/btcd/btcec/v2 v2.2.0 // indirect
	github.com/ethereum/go-ethereum v1.10.17
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.21.0
	golang.org/x/crypto v0.0.0-20220518034528-6f7dac969898 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
)
