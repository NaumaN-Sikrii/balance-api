package main

import (
	"flag"
	"fmt"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/NaumaN-Sikrii/balance-api/pkg/balance"
)

func getUserPause() (parsedUser string, frequency int, err error) {
	flag.IntVar(&frequency, "freq", 30, "[-frequency]")
	flag.StringVar(&parsedUser, "user", "", "[-user]")
	flag.Parse()

	isAddress := common.IsHexAddress(parsedUser)
	if !isAddress {
		return parsedUser, frequency, fmt.Errorf("Invalid hex user provided: %w", parsedUser)
	}
	return
}

// Init contract read-only caller
func initCaller(client bind.ContractCaller, addr string) (*balance.BalanceCaller, error) {
	isAddress := common.IsHexAddress(addr)
	if !isAddress {
		return nil, fmt.Errorf("Invalid contract address: %w", addr)
	}
	return balance.NewBalanceCaller(common.HexToAddress(addr), client)
}
