package main

import (
	"time"

	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/NaumaN-Sikrii/balance-api/internal"
	"go.uber.org/zap"
)

const (
	snMainnetURL = "https://speedy-nodes-nyc.moralis.io/d4347f1973041b37f0205b1f/eth/mainnet"
	contractAddr = "0xdAC17F958D2ee523a2206206994597C13D831ec7"
)

func main() {
	// init logger
	logger, _ := zap.NewDevelopment()
	defer logger.Sync()
	sugar := logger.Sugar()

	// init speedy node connection
	client, err := ethclient.Dial(snMainnetURL)
	if err != nil {
		sugar.Fatalf("Can not init mainnet connection: %v", err)
	}
	defer client.Close()

	balanceCaller, err := initCaller(client, contractAddr)
	if err != nil {
		sugar.Fatalf("Can not init caller: %v", err)
	}

	// Get user hex from flag
	user, frequency, err := getUserPause()
	if err != nil {
		sugar.Fatal(err)
	}

	svc := internal.NewService(balanceCaller, sugar)
	svc.GetBalancePerTime(time.Duration(frequency), user)
}
