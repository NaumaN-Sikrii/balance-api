package internal

import (
	"fmt"
	"time"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/NaumaN-Sikrii/balance-api/pkg/balance"
	"go.uber.org/zap"
)

type BalanceReader interface {
	// Printing balance of user (USDT) at a frequency of provided seconds
	GetBalancePerTime(frequency time.Duration, user string)
}

type Service struct {
	caller *balance.BalanceCaller
	logger *zap.SugaredLogger
}

func NewService(caller *balance.BalanceCaller, logger *zap.SugaredLogger) Service {
	return Service{caller: caller, logger: logger}
}

func (s *Service) GetBalancePerTime(secs time.Duration, user string) {
	for range time.Tick(secs * time.Second) {
		userBalance, err := s.caller.BalanceOf(nil, common.HexToAddress(user))
		if err != nil {
			s.logger.Fatalf("error in BalanceOf: %w", err)
		}
		fmt.Printf("Balance is %v for user: %s\n", userBalance, user)
	}
}
