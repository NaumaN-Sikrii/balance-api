# Balance Api

## Getting started

Balance-api helps you to monitor the user's balance (**USDT**)

Fork and clone on your computer!

## Build

**Docker:**
```
 sudo docker build -t balance .
```

**Locally:**

**Go** version >= 1.16.4
```
cd /balance-api
go mod tidy 
go build ./cmd/...
```

## Run

There is to flags you need to provide

- **user** - hex address of user
- **freq** - frequency of printing balance in wei (optional, default _30s_)

Finally, run it:

**Docker:**

```
[comment]: example
sudo docker run <image_id> -user 0x0d4a11d5EEaaC28EC3F61d100daF4d40471f1852 -freq 30
```

**Locally:**
```
[comment]: example 
 go run ./cmd -user 0x0d4a11d5EEaaC28EC3F61d100daF4d40471f1852 -freq 30
```
In this case we will get actual balance of user every 30 sec

Output example
```
Balance is 43586284993573 for user: 0x0d4a11d5EEaaC28EC3F61d100daF4d40471f1852
Balance is 43586284993573 for user: 0x0d4a11d5EEaaC28EC3F61d100daF4d40471f1852
Balance is 43605516921733 for user: 0x0d4a11d5EEaaC28EC3F61d100daF4d40471f1852
```